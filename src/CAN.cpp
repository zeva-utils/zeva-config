#include "CAN.hpp"

#include <cstring>
#include <error.h>
#include <linux/can.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

CAN::CAN(std::string port) : port(port)
{
	openPort();
}

CAN::~CAN()
{
	closePort();
}

void CAN::write(uint32_t can_id, uint8_t data_length, uint8_t *data)
{
	// Check for valid payload
	if (CAN_MAX_DLEN < data_length)
	{
		error_at_line(-1, 0, __FILE__, __LINE__, "Max CAN frame payload length is %u bytes, tried to send %u", CAN_MAX_DLEN, data_length);
	}
	if (!data && data_length)
	{
		error_at_line(-1, 0, __FILE__, __LINE__, "Pointer to CAN frame payload was null, but payload length was non-zero");
	}

	// Construct CAN frame
	can_frame frame;
	frame.can_id = can_id;
	frame.can_dlc = data_length;
	for (int i = 0; i < data_length; i++)
	{
		frame.data[i] = data[i];
	}

	// Send frame
	if (::write(socket_fd, &frame, sizeof(can_frame)) != sizeof(can_frame))
	{
		error_at_line(errno, errno, __FILE__, __LINE__, "write");
	}
}

void CAN::openPort()
{
	socket_fd = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if (socket_fd < 0)
	{
		error_at_line(errno, errno, __FILE__, __LINE__, "socket");
	}

	ifreq ifr;
	if (snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", port.c_str()) < 0)
	{
		error_at_line(errno, errno, __FILE__, __LINE__, "snprintf");
	}
	if (ioctl(socket_fd, SIOCGIFINDEX, &ifr) < 0)
	{
		error_at_line(errno, errno, __FILE__, __LINE__, "%s", port.c_str());
	}

	sockaddr_can addr;
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;
	if (bind(socket_fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		error_at_line(errno, errno, __FILE__, __LINE__, "bind");
	}
}

void CAN::closePort()
{
	if (close(socket_fd) < 0)
	{
		error_at_line(errno, errno, __FILE__, __LINE__, "close");
	}
}